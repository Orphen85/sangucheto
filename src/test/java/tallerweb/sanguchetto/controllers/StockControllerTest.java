package tallerweb.sanguchetto.controllers;

import java.util.Map;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;

import tallerweb.sangucheto.controllers.StockController;
import tallerweb.sangucheto.model.Ingrediente;
import tallerweb.sangucheto.model.TipoIngrediente;
import tallerweb.sangucheto.utils.Stock;

public class StockControllerTest {
	private StockController miController;
	private String viewName;
	private ModelAndView view;

	@Before
	public void setup() {
		miController = new StockController();
	}

	@After
	public void VerificacionNombreVista() {
		Assert.assertEquals(view.getViewName(), viewName);
		limpieza();
	}

	public void limpieza() {
		viewName = null;
		view = null;
		miController = null;
	}

	@Test
	public void crearIngrediente() {
		// Preparacion
		Ingrediente ingrediente = (Ingrediente) Stock.getInstance()
				.listarIngredientesDisponibles().toArray()[0];
		viewName = "stock/stock";
		// Ejecucion
		view = miController.crearIngrediente(ingrediente);
		// Verificacion
		Map<String, Object> model = view.getModel();
		Assert.assertTrue(model.containsKey("stock"));
		Assert.assertTrue(model.containsKey("estado"));
		// Limpieza
		ingrediente = null;
	}

	@Test
	public void agregarIngrediente() {
		// Preparacion
		viewName = "stock/agregar";
		// Ejecucion
		view = miController.agregarIngrediente();
	}

	@Test
	public void agregarIngredienteExistentePost() {
		// Preparacion
		Ingrediente ingrediente = (Ingrediente) Stock.getInstance()
				.listarIngredientesDisponibles().toArray()[0];
		viewName = "stock/agregar";
		String mensajeEsperado = "Ingrediente existente";
		// Ejecucion
		view = miController.agregarIngredientePost(ingrediente);
		// Verificacion
		Map<String, Object> model = view.getModel();
		Assert.assertTrue(model.containsKey("error"));
		Assert.assertEquals(mensajeEsperado, model.get("mensaje"));
		// Limpieza
		ingrediente = null;
		mensajeEsperado = null;
	}

	@Test
	public void agregarIngredienteInvalidoPost() {
		// Preparacion
		Ingrediente ingrediente = new Ingrediente();
		viewName = "stock/agregar";
		String mensajeEsperado = "Complete los datos obligatorios";
		// Ejecucion
		view = miController.agregarIngredientePost(ingrediente);
		// Verificacion
		Map<String, Object> model = view.getModel();
		Assert.assertTrue(model.containsKey("error"));
		Assert.assertEquals(mensajeEsperado, model.get("mensaje"));
		// Limpieza
		ingrediente = null;
		mensajeEsperado = null;
	}

	@Test
	public void agregarIngredientePost() {
		// Preparacion
		Ingrediente ingrediente = new Ingrediente("Bondiola", 5.0,
				TipoIngrediente.INGREDIENTE);
		viewName = "stock/stock";
		// Ejecucion
		view = miController.agregarIngredientePost(ingrediente);
		// Verificacion
		// Limpieza
		ingrediente = null;
	}

	@Test
	public void agregarCantidad() {
		// Preparacion
		viewName = "stock/agregarCantidad";
		// Ejecucion
		view = miController.agregarCantidad();
	}

	@Test
	public void agregarCantidadPost() {
		// Preparacion
		String ingredienteNoExistente = "Bondiola";
		Integer cantidad = 1;
		viewName = "stock/stock";
		// Ejecucion
		view = miController.agregarCantidadPost(ingredienteNoExistente,
				cantidad);
		// Verificacion
		// Limpieza
		ingredienteNoExistente = null;
		cantidad = null;
	}

	@Test
	public void agregarCantidadIngredienteExistenteConCantidadPost() {
		// Preparacion
		String ingredienteExistente = ((Ingrediente) Stock.getInstance()
				.listarIngredientesDisponibles().toArray()[0]).getNombre();
		Integer cantidad = 1;
		viewName = "stock/stock";
		// Ejecucion
		view = miController.agregarCantidadPost(ingredienteExistente, cantidad);
		// Verificacion
		// Limpieza
		ingredienteExistente = null;
		cantidad = null;
	}

	@Test
	public void agregarCantidadIngredienteExistenteSinCantidadPost() {
		// Preparacion
		String ingredienteExistente = ((Ingrediente) Stock.getInstance()
				.listarIngredientesDisponibles().toArray()[0]).getNombre();
		Integer cantidad = 0;
		String mensajeEsperado = "Ingrese una cantidad mayor a 0";
		viewName = "stock/agregarCantidad";
		// Ejecucion
		view = miController.agregarCantidadPost(ingredienteExistente, cantidad);
		// Verificacion
		Map<String, Object> model = view.getModel();
		Assert.assertTrue(model.containsKey("error"));
		Assert.assertEquals(mensajeEsperado, model.get("mensaje"));
		// Limpieza
		ingredienteExistente = null;
		cantidad = null;
		mensajeEsperado = null;
		model = null;
	}

	@Test
	public void eliminar() {
		// Preparacion
		viewName = "stock/eliminar";
		// Ejecucion
		view = miController.eliminar();
	}

	@Test
	public void eliminarIngrediente() {
		// Preparacion
		String ingrediente = ((Ingrediente) Stock.getInstance()
				.listarIngredientesDisponibles().toArray()[0]).getNombre();
		viewName = "stock/stock";
		// Ejecucion
		view = miController.eliminarIngrediente(ingrediente);
		// Verificacion
		// Limpieza
		ingrediente = null;
	}

	@Test
	public void mostrarStock() {
		// Preparacion
		viewName = "stock/stock";
		// Ejecucion
		view = miController.mostrarStock();
	}
}