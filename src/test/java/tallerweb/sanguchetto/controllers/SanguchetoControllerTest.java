package tallerweb.sanguchetto.controllers;

import java.util.Map;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;

import tallerweb.sangucheto.controllers.SanguchetoController;
import tallerweb.sangucheto.model.Ingrediente;
import tallerweb.sangucheto.utils.Stock;

public class SanguchetoControllerTest {

	private SanguchetoController miController;
	private String viewName;
	private ModelAndView view;

	@Before
	public void setup(){
		miController = new SanguchetoController();
	}
	
	@After
	public void VerificacionNombreVista(){
		Assert.assertEquals(view.getViewName(), viewName);
		limpieza();
	}
	
	public void limpieza(){
		viewName = null;
		view = null;
	}
	
	@Test
	public void mostrarPedidoGet(){
		//Preparacion
		viewName = "sangucheto/pedido";
		//Ejecucion
		view = miController.mostrarPedido();
		//Verificacion
		Map<String, Object> model = view.getModel();
		Assert.assertTrue(model.containsKey("stock"));
		Assert.assertTrue(model.containsKey("condimentos"));
		Assert.assertTrue(model.containsKey("ingredientes"));
		Assert.assertTrue(model.containsKey("total"));
		
	}
	
	@Test
	public void confirmarPedidoPost(){
		//Preparacion
		viewName = "sangucheto/confirmacion";
		//Ejecucion
		view = miController.confirmarPedido();
		//Verificacion
		Map<String, Object> model = view.getModel();
		Assert.assertTrue(model.containsKey("condimentos"));
		Assert.assertTrue(model.containsKey("ingredientes"));
		Assert.assertTrue(model.containsKey("total"));
	}
	
	@Test
	public void vaciar(){
		//Preparacion
		viewName = "inicio";
		//Ejecucion
		view = miController.vaciar();
	}
	
	@Test
	public void agregarIngrediente(){
		//Preparacion
		Ingrediente ingrediente = (Ingrediente) Stock.getInstance().listarIngredientesDisponibles().toArray()[0];
		viewName = "sangucheto/pedido";
		//Ejecucion
		view = miController.agregarIngrediente(ingrediente.getNombre());
		//Verificacion
		Map<String, Object> model = view.getModel();
		Assert.assertTrue(model.containsKey("stock"));
		Assert.assertTrue(model.containsKey("condimentos"));
		Assert.assertTrue(model.containsKey("ingredientes"));
		Assert.assertTrue(model.containsKey("total"));
		//Limpieza
		ingrediente = null;
	}

	@Test
	public void eliminarIngrediente(){
		//Preparacion
		Ingrediente ingrediente = new Ingrediente();
		viewName = "sangucheto/pedido";
		//Ejecucion
		view = miController.eliminarIngrediente(ingrediente.getNombre());
		//Verificacion
		Map<String, Object> model = view.getModel();
		Assert.assertTrue(model.containsKey("stock"));
		Assert.assertTrue(model.containsKey("condimentos"));
		Assert.assertTrue(model.containsKey("ingredientes"));
		Assert.assertTrue(model.containsKey("total"));
		//Limpieza
		ingrediente = null;
	}
}
