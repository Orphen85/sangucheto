package tallerweb.sanguchetto.utils;

import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import tallerweb.sangucheto.model.Ingrediente;
import tallerweb.sangucheto.model.TipoIngrediente;
import tallerweb.sangucheto.utils.Sanguchetto;

public class SanguchettoTest {

	private Sanguchetto sanguchetto;
	
	@Before
	public void configuracion(){
		sanguchetto = Sanguchetto.getInstance();
	}

	@After
	public void limpieza(){
		sanguchetto.vaciar();
	}

	@Test
    public void testVaciar() {
        //Configuracion
    	sanguchetto.agregarIngrediente(new Ingrediente());
    	//Ejecucion
    	sanguchetto.vaciar();
    	//Verificacion
    	Assert.assertTrue(sanguchetto.verCondimentos().isEmpty() && sanguchetto.verIngredientes().isEmpty());
    }

    @Test
    public void testAgregarIngrediente() {
    	//Ejecucion
    	Ingrediente ingrediente = new Ingrediente();
    	ingrediente.setTipo(TipoIngrediente.CONDIMENTO);
    	sanguchetto.agregarIngrediente(ingrediente);
    	//Verificacion
    	Assert.assertFalse(sanguchetto.verCondimentos().isEmpty());
    	//Limpieza
    	ingrediente = null;
    }

    @Test
    public void testVerIngredientes() {
    	//Configuracion
    	Ingrediente ingredienteEsperado = new Ingrediente();
    	ingredienteEsperado.setTipo(TipoIngrediente.INGREDIENTE);
    	sanguchetto.agregarIngrediente(ingredienteEsperado);
    	//Ejecucion
    	List<Ingrediente> ingredientesActuales = sanguchetto.verIngredientes();
    	//Verificacion
    	Assert.assertTrue(ingredientesActuales.contains(ingredienteEsperado));
    	//Limpieza
    	ingredienteEsperado = null;
    }

    @Test
    public void testVerCondimentos() {
    	//Configuracion
    	Ingrediente ingredienteEsperado = new Ingrediente();
    	ingredienteEsperado.setTipo(TipoIngrediente.CONDIMENTO);
    	sanguchetto.agregarIngrediente(ingredienteEsperado);
    	//Ejecucion
    	List<Ingrediente> ingredientesActuales = sanguchetto.verCondimentos();
    	//Verificacion
    	Assert.assertTrue(ingredientesActuales.contains(ingredienteEsperado));
    	//Limpieza
    	ingredienteEsperado = null;
    }

    @Test
    public void testGetPrecio() {
    	//Configuracion
    	Ingrediente ingrediente = new Ingrediente();
    	Double precioEsperado = 10.0;
    	ingrediente.setPrecio(precioEsperado);
    	sanguchetto.agregarIngrediente(ingrediente);
    	//Ejecucion
    	Double precioaActual = sanguchetto.getPrecio();
    	//Verificacion
    	Assert.assertEquals(precioEsperado, precioaActual);
    	//Limpieza
    	ingrediente = null;
    	precioEsperado = null;
    }
    
    @Test
    public void eliminarIngredienteTest(){
    	//Configuracion
    	Ingrediente ingrediente = new Ingrediente();
    	sanguchetto.agregarIngrediente(ingrediente);
    	//Ejecucion
    	//Verificacion
    	Assert.assertTrue(sanguchetto.eliminarIngrediente(ingrediente));
    	//Limpieza
    	ingrediente = null;
    }
}
