package tallerweb.sanguchetto.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import tallerweb.sangucheto.model.Ingrediente;
import tallerweb.sangucheto.model.TipoIngrediente;
import tallerweb.sangucheto.utils.Stock;

public class StockTest {
	
	private Stock miStock = Stock.getInstance();

	@Test
	public void listarIngredientesDisponibles(){
		//Preparacion
		Ingrediente ingredienteEsperado = new Ingrediente("Papas", 0.1, TipoIngrediente.CONDIMENTO);
		miStock.agregarIngrediente(ingredienteEsperado);
		//Ejecucion
		Set<Ingrediente> lista = miStock.listarIngredientesDisponibles();
		//Verificacion
		Assert.assertTrue(lista.contains(ingredienteEsperado));
		//Limpieza
		lista = null;
		ingredienteEsperado = null;
		miStock.eliminarIngrediente(ingredienteEsperado);
	}
	
	@Test
	public void obtenerStock(){
		//Preparacion
		Ingrediente ingredienteEsperado = new Ingrediente("Papas", 0.1, TipoIngrediente.CONDIMENTO);
		//Ejecucion
		//Verificacion
		Assert.assertFalse(miStock.obtenerStock().containsValue(ingredienteEsperado));
		miStock.agregarIngrediente(ingredienteEsperado);
		miStock.agregarStock(ingredienteEsperado, 1);
		Assert.assertTrue(miStock.obtenerStock().containsKey(ingredienteEsperado));
		//Limpieza
		ingredienteEsperado = null;
		miStock.eliminarIngrediente(ingredienteEsperado);
	}
	
	@Test
	public void obtenerIngredientePorNombre(){
		//Preparacion
		String nombre = "Jamon crudo";
		Ingrediente ingredienteEsperado = new Ingrediente(nombre, 0.1, TipoIngrediente.INGREDIENTE);
		miStock.agregarIngrediente(ingredienteEsperado);
		//Ejecucion
		Ingrediente ingredienteActual = miStock.obtenerIngredientePorNombre(nombre);
		//Verificacion
		Assert.assertEquals(ingredienteEsperado, ingredienteActual);
		//Limpieza
		ingredienteEsperado = ingredienteActual = null;
		nombre = null;
		miStock.eliminarIngrediente(ingredienteEsperado);
	}
	
	@Test
	public void obtenerStockDisponible(){
		//Preparacion
		Integer stockDisponibleEsperado = 12;
		Ingrediente ingrediente = new Ingrediente("Huevo", 0.1, TipoIngrediente.CONDIMENTO);
		miStock.agregarIngrediente(ingrediente);
		miStock.agregarStock(ingrediente, stockDisponibleEsperado);
		//Ejecucion
		Integer stockDisponibleActual = miStock.obtenerStockDisponible(ingrediente);
		//Verificacion
		Assert.assertEquals(stockDisponibleEsperado, stockDisponibleActual);
		//Limpieza
		ingrediente = null;
		stockDisponibleActual = stockDisponibleEsperado = null;
		miStock.eliminarIngrediente(ingrediente);
	}
	
	@Test
	public void obtenerStockDisponibleDeIngredienteSinStock(){
		//Preparacion
		Integer stockDisponibleEsperado = null;
		Ingrediente ingrediente = new Ingrediente("obtener Stock Disponible", 0.1, TipoIngrediente.CONDIMENTO);
		//Ejecucion
		Integer stockDisponibleActual = miStock.obtenerStockDisponible(ingrediente);
		//Verificacion
		Assert.assertEquals(stockDisponibleEsperado, stockDisponibleActual);
		//Limpieza
		ingrediente = null;
		stockDisponibleActual = stockDisponibleEsperado = null;
		miStock.eliminarIngrediente(ingrediente);
	}
	
	@Test
	public void agregarIngrediente(){
		//Preparacion
		Ingrediente ingrediente = new Ingrediente("Huevo no existente", 0.1, TipoIngrediente.CONDIMENTO);
		//Ejecucion
		//Verificacion
		Assert.assertTrue(miStock.agregarIngrediente(ingrediente));
		//Limpieza
		ingrediente = null;
		miStock.eliminarIngrediente(ingrediente);
	}
	
	@Test
	public void agregarIngredienteExistente(){
		//Preparacion
		Ingrediente ingrediente = new Ingrediente("Huevo Existente", 0.1, TipoIngrediente.CONDIMENTO);
		miStock.agregarIngrediente(ingrediente);
		//Ejecucion
		//Verificacion
		Assert.assertFalse(miStock.agregarIngrediente(ingrediente));
		//Limpieza
		ingrediente = null;
		miStock.eliminarIngrediente(ingrediente);
	}
	
	@Test
	public void agregarIngredientes(){
		//Preparacion
		Ingrediente ingrediente1 = new Ingrediente("Ingrediente1", 0.1, TipoIngrediente.CONDIMENTO);
		Ingrediente ingrediente2 = new Ingrediente("Ingrediente2", 0.1, TipoIngrediente.CONDIMENTO);
		List<Ingrediente> ingredientes = new ArrayList<Ingrediente>();
		ingredientes.add(ingrediente1);
		ingredientes.add(ingrediente2);
		//Ejecucion
		miStock.agregarIngredientes(ingredientes);
		//Verificacion
		Assert.assertTrue(miStock.listarIngredientesDisponibles().contains(ingrediente1));
		Assert.assertTrue(miStock.listarIngredientesDisponibles().contains(ingrediente2));
		//Limpieza
		ingrediente1 = ingrediente2 = null;
		miStock.eliminarIngrediente(ingrediente1);
		miStock.eliminarIngrediente(ingrediente2);
	}
	
	@Test
	public void comprarIngredienteInexistente(){
		//Preparacion
		Ingrediente ingrediente = new Ingrediente("Lomo Inexistente", 2.0, TipoIngrediente.INGREDIENTE);
		Integer cantidad = 10;
		//Ejecucion
		//Verificacion
		Assert.assertFalse(miStock.comprarIngrediente(ingrediente, cantidad));
		//Limpieza
		ingrediente = null;
		cantidad = null;
		miStock.eliminarIngrediente(ingrediente);
	}
	
	@Test
	public void comprarIngredienteExistente(){
		//Preparacion
		Ingrediente ingrediente = new Ingrediente("Lomo Existente", 2.0, TipoIngrediente.INGREDIENTE);
		Integer cantidad = 10;
		miStock.agregarIngrediente(ingrediente);
		//Ejecucion
		//Verificacion
		Assert.assertTrue(miStock.comprarIngrediente(ingrediente, cantidad));
		//Limpieza
		ingrediente = null;
		cantidad = null;
		miStock.eliminarIngrediente(ingrediente);
	}

	@Test
	public void existeIngredienteExistente(){
		//Preparacion
		Ingrediente ingrediente = new Ingrediente("existe ingrediente", 2.0, TipoIngrediente.INGREDIENTE);
		miStock.agregarIngrediente(ingrediente);
		//Ejecucion
		//Verificacion
		Assert.assertTrue(miStock.existeIngrediente(ingrediente));
		//Limpieza
		ingrediente = null;
		miStock.eliminarIngrediente(ingrediente);
	}

	@Test
	public void existeIngredienteInexistente(){
		//Preparacion
		Ingrediente ingrediente = new Ingrediente("No existe ingrediente", 2.0, TipoIngrediente.INGREDIENTE);
		//Ejecucion
		//Verificacion
		Assert.assertFalse(miStock.existeIngrediente(ingrediente));
		//Limpieza
		ingrediente = null;
		miStock.eliminarIngrediente(ingrediente);
	}
}