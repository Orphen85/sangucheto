package tallerweb.sangucheto.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import tallerweb.sangucheto.model.Ingrediente;
import tallerweb.sangucheto.model.TipoIngrediente;

/**
 * Maneja un stock de ingredientes, el mismo puede ser asociado a una cantidad.<br>
 * No persiste, es decir, luego de la ejecucion del programa el Stock se inicialza vacio.<br>
 * @author sismael
 *
 */
public class Stock {
	
	private static Stock instance = new Stock();
	private Map<Ingrediente, Integer> stock = new HashMap<Ingrediente, Integer>();
	
	private Stock(){
		Ingrediente jamon = new Ingrediente("Jamón", 1.0, TipoIngrediente.INGREDIENTE);
		stock.put(jamon, 10);
		Ingrediente queso = new Ingrediente("Queso", 0.5, TipoIngrediente.INGREDIENTE);
		stock.put(queso, 1);
		Ingrediente salame = new Ingrediente("Salame", 1.0, TipoIngrediente.INGREDIENTE);
		stock.put(salame, 20);
		Ingrediente pollo = new Ingrediente("Pollo", 2.0, TipoIngrediente.INGREDIENTE);
		stock.put(pollo, 5);
		Ingrediente ketchup = new Ingrediente("Ketchup", 0.25, TipoIngrediente.CONDIMENTO);
		stock.put(ketchup, 1);
	}

	public static Stock getInstance(){
		return instance;
	}
	
	/**
	 * Devuelve un listado de los ingredientes del stock, tengan o no stock, es decir, los ingredientes con cantidad 0 son incluidos.<br>
	 * @param producto
	 * @param cantidad
	 * @return 
	 */
	public Set<Ingrediente> listarIngredientesDisponibles(){
		return stock.keySet();
	}
	
	/**
	 * Devuelve un mapa con los ingredientes y su stock correspondiente, tengan o no stock, es decir, los ingredientes con cantidad 0 son incluidos.<br>
	 * @param producto
	 * @param cantidad
	 * @return 
	 */
	public Map<Ingrediente, Integer> obtenerStock(){
		return stock;
	}
	
	public Ingrediente obtenerIngredientePorNombre(String nombre){
		Set<Ingrediente> ingredientes = this.listarIngredientesDisponibles();
		for (Iterator<Ingrediente> iterator = ingredientes.iterator(); iterator.hasNext();) {
			Ingrediente ingrediente = (Ingrediente) iterator.next();
			if(ingrediente.getNombre().equals(nombre)){
				return ingrediente;
			}
		}
		return null;
	}
	
	/**
	 * Permite agregar el ingrediente indicado al stock, con cantidad 0.<br>
	 * @param ingrediente
	 * @param cantidad
	 * @return true en caso de exito, false si el ingrediente ya existe en el stock.<br>
	 */
	public Boolean agregarIngrediente(Ingrediente ingrediente){
		if(this.stock.containsKey(ingrediente)){
			return false;
		}
		this.stock.put(ingrediente, 0);
		return true;
	}

	public void agregarIngredientes(List<Ingrediente> ingredientes){
		for (Iterator<Ingrediente> iterator = ingredientes.iterator(); iterator.hasNext();) {
			Ingrediente ingrediente = iterator.next();
			if(! this.stock.containsKey(ingrediente)){
				this.agregarIngrediente(ingrediente);
			}
			this.agregarStock(ingrediente, 1);
		}
	}
	
	/**
	 * Permite agregar stock al existente para un ingrediente dado. Si el ingrediente tiene un stock de N, ahora tendra N + cantidad.<br>
	 * @param ingrediente
	 * @param cantidad
	 * @return true en caso de exito, false si el ingrediente no existe en el stock.<br>
	 */
	public Boolean agregarStock(Ingrediente ingrediente, Integer cantidad){
		if(!this.stock.containsKey(ingrediente)){
			return false;
		}
		Integer nuevaCantidad = this.stock.get(ingrediente) + cantidad;
		this.stock.put(ingrediente, nuevaCantidad);
		return true;
	}
	
	/**
	 * Devuelve el stock disponible para el ingrediente pedido. NULL si el ingrediente no existe en el stock<br>
	 * @param ingrediente
	 * @return
	 */
	public Integer obtenerStockDisponible(Ingrediente ingrediente){
		if(!this.stock.containsKey(ingrediente)){
			return null;
		}
		return this.stock.get(ingrediente);
	}
	
	/**
	 * Indica si el ingrediente indicado fue incluido en el stock.<br>
	 * @param ingrediente
	 * @return
	 */
	public Boolean existeIngrediente(Ingrediente ingrediente){
		return this.stock.containsKey(ingrediente);
	}
	
	/**
	 * Permite comprar N unidades del ingrediente indicado.<br>
	 * @param ingrediente
	 * @param unidades
	 * @return true en caso de exito, false si el ingrediente no existe en el stock.<br>
	 */
	public Boolean comprarIngrediente(Ingrediente ingrediente, Integer unidades){
		if(!this.stock.containsKey(ingrediente)){
			return false;
		}
		Integer nuevaCantidad = this.stock.get(ingrediente) - unidades;
		this.stock.put(ingrediente, nuevaCantidad);
		return true;
	}
	
	/**
	 * Elimina el ingrediente indicado del stock, no importa cual sea la cantidad disponible del mismo.<br>
	 * @param ingrediente
	 * @return true en caso de exito, false si el ingrediente no existe en el stock.<br>
	 */
	public Boolean eliminarIngrediente(Ingrediente ingrediente){
		if(!this.stock.containsKey(ingrediente)){
			return false;
		}
		this.stock.remove(ingrediente);
		return true;
	}
}
