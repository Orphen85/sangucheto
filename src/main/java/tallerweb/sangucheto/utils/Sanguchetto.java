package tallerweb.sangucheto.utils;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import tallerweb.sangucheto.model.Ingrediente;
import tallerweb.sangucheto.model.TipoIngrediente;

public class Sanguchetto {

	private static Sanguchetto instance = new Sanguchetto();
	private List<Ingrediente> ingredientes = new LinkedList<Ingrediente>();
	
	private Sanguchetto(){}
	
	public static Sanguchetto getInstance(){
		return instance;
	}
	
	/**
	 * Elimina todos los ingredientes del sanguchetto.<br>
	 */
	public void vaciar(){
		ingredientes.clear();
	}
	
	/**
	 * Agrega un ingrediente al carrito.<br>
	 * @param ingrediente
	 */
	public void agregarIngrediente(Ingrediente ingrediente){
		ingredientes.add(ingrediente);
	}
	
	/**
	 * Lista todos los ingredientes que forman parte del sanguchetto.<br>
	 * @return
	 */
	public List<Ingrediente> verIngredientes(){
		return obtenerIngredientePorTipo(TipoIngrediente.INGREDIENTE);
	}
	

	public List<Ingrediente> obtenerIngredientes(){
		return ingredientes;
	}
	
	
	/**
     * Lista todos los condimentos que forman parte del sanguchetto.<br>
     * @return
     */
    public List<Ingrediente> verCondimentos(){
    	return obtenerIngredientePorTipo(TipoIngrediente.CONDIMENTO);
    }
	
	/**
	 * Devuelve el precio total del sanguchetto.<br>
	 * @return
	 */
	public Double getPrecio(){
		Double total = 0.0;
		for (Iterator<Ingrediente> iterator = ingredientes.iterator(); iterator.hasNext();) {
			Ingrediente ingrediente = iterator.next();
			total += ingrediente.getPrecio();
		}
		return total;
	}
	
	public boolean eliminarIngrediente(Ingrediente ingrediente) {
		return ingredientes.remove(ingrediente);
	}
	/***********PRIVATE METHODS***************************************************************************************/
	private List<Ingrediente> obtenerIngredientePorTipo(TipoIngrediente tipoIngrediente){
		List<Ingrediente> ingredientesARetornar = new LinkedList<Ingrediente>();
		for (Iterator<Ingrediente> iterator = ingredientes.iterator(); iterator.hasNext();) {
			Ingrediente ingrediente = iterator.next();
			if(ingrediente.getTipo() == tipoIngrediente){
				ingredientesARetornar.add(ingrediente);
			}
		}
		return ingredientesARetornar;
	}
}
