package tallerweb.sangucheto.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import tallerweb.sangucheto.model.Ingrediente;
import tallerweb.sangucheto.utils.Stock;

@Controller
public class StockController {

	private Stock stock = Stock.getInstance();

	@RequestMapping(value = "/stock", method = RequestMethod.GET)
	public ModelAndView mostrarStock() {
		return new ModelAndView("stock/stock", "stock", stock.obtenerStock());
	}

	@RequestMapping(value = "/stock", method = RequestMethod.POST)
	public ModelAndView crearIngrediente(
			@ModelAttribute("ingrediente") Ingrediente ingrediente) {

		ModelAndView vista = new ModelAndView("stock/stock");
		Boolean estado = false;
		if (!ingrediente.getNombre().isEmpty()
				|| !ingrediente.getPrecio().isNaN()
				|| !(ingrediente.getTipo() == null)) {
			estado = ! stock.agregarIngrediente(ingrediente);
			vista.addObject("estado", estado);
			vista.addObject("stock", stock.obtenerStock());
		}
		return vista;
	}

	@RequestMapping(value = "/ingrediente", method = RequestMethod.GET)
	public ModelAndView agregarIngrediente() {
		return new ModelAndView("stock/agregar");
	}

	@RequestMapping(value = "/ingrediente", method = RequestMethod.POST)
	public ModelAndView agregarIngredientePost(
			@ModelAttribute("ingrediente") Ingrediente ingrediente) {
		
		ModelAndView vista = new ModelAndView("stock/agregar");

		if(ingrediente.getNombre() == "" || ingrediente.getPrecio() == null){
			vista.addObject("error", Boolean.TRUE);
			vista.addObject("mensaje", "Complete los datos obligatorios");
		}else{
			if(stock.agregarIngrediente(ingrediente)){
				vista =  new ModelAndView("stock/stock", "stock", stock.obtenerStock());
			}else{
				vista.addObject("error", Boolean.TRUE);
				vista.addObject("mensaje", "Ingrediente existente");
			}
		}
		return vista;
	}
	
	@RequestMapping(value = "/stock/agregarCantidad", method = RequestMethod.GET)
	public ModelAndView agregarCantidad() {
		return new ModelAndView("stock/agregarCantidad", "stock", stock.obtenerStock());
	}

	@RequestMapping(value = "/stock/agregarCantidad", method = RequestMethod.POST)
	public ModelAndView agregarCantidadPost(
			@RequestParam("ingrediente") String nombreIngrediente, 
			@RequestParam("cantidad") Integer cantidad) {
		
		ModelAndView vista = new ModelAndView("stock/stock", "stock", stock.obtenerStock());

		Ingrediente ingrediente = stock
				.obtenerIngredientePorNombre(nombreIngrediente);
		
		if (ingrediente != null) {
			if(cantidad > 0){
				stock.agregarStock(ingrediente, cantidad);
			}else{
				vista = new ModelAndView("stock/agregarCantidad");
				vista.addObject("error", Boolean.TRUE);
				vista.addObject("mensaje", "Ingrese una cantidad mayor a 0");
			}
		}
		
		return vista;
	}

	@RequestMapping(value = "/stock/eliminar", method = RequestMethod.GET)
	public ModelAndView eliminar() {
		return new ModelAndView("stock/eliminar", "stock", stock.obtenerStock());
	}

	@RequestMapping(value = "/stock/eliminar", method = RequestMethod.POST)
	public ModelAndView eliminarIngrediente(
			@RequestParam("ingrediente") String nombreIngrediente) {
		Ingrediente ingrediente = stock
				.obtenerIngredientePorNombre(nombreIngrediente);
		if (ingrediente != null) {
			stock.eliminarIngrediente(ingrediente);
		}

		return new ModelAndView("stock/stock", "stock", stock.obtenerStock());
	}

}