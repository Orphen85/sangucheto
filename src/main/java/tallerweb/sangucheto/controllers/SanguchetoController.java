package tallerweb.sangucheto.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import tallerweb.sangucheto.model.Ingrediente;
import tallerweb.sangucheto.utils.Sanguchetto;
import tallerweb.sangucheto.utils.Stock;

@Controller
public class SanguchetoController {

	private Stock stock = Stock.getInstance();
	private Sanguchetto sanguchetto = Sanguchetto.getInstance();
	
	@RequestMapping(value = "/pedido", method = RequestMethod.GET)
	public ModelAndView mostrarPedido() {
		ModelAndView vista = new ModelAndView("sangucheto/pedido");
		vista.addObject("stock", stock.obtenerStock());
		vista.addObject("condimentos", sanguchetto.verCondimentos());
		vista.addObject("ingredientes", sanguchetto.verIngredientes());
		vista.addObject("total", sanguchetto.getPrecio());
		
		return vista;
	}
	
	@RequestMapping(value = "/pedido", method = RequestMethod.POST)
	public ModelAndView confirmarPedido() {
		ModelAndView vista = new ModelAndView("sangucheto/confirmacion");
		vista.addObject("condimentos", sanguchetto.verCondimentos());
		vista.addObject("ingredientes", sanguchetto.verIngredientes());
		vista.addObject("total", sanguchetto.getPrecio());
		
		sanguchetto.vaciar();

		return vista;
	}
	
	@RequestMapping(value = "/pedido/vaciar", method = RequestMethod.POST)
	public ModelAndView vaciar() {
		ModelAndView vista = new ModelAndView("inicio");
		stock.agregarIngredientes(sanguchetto.obtenerIngredientes());
		sanguchetto.vaciar();
		
		return vista;
	}
	
	@RequestMapping(value = "/agregarIngrediente", method = RequestMethod.POST)
	public ModelAndView agregarIngrediente(@RequestParam(value="ingrediente") String nombre) {
		
		ModelAndView vista = new ModelAndView("sangucheto/pedido");
		
		Ingrediente ingrediente = stock.obtenerIngredientePorNombre(nombre);
		if(stock.obtenerStockDisponible(ingrediente) > 0){
			stock.comprarIngrediente(ingrediente, 1);
			sanguchetto.agregarIngrediente(ingrediente);
		}
		
		vista.addObject("stock", stock.obtenerStock());
		vista.addObject("condimentos", sanguchetto.verCondimentos());
		vista.addObject("ingredientes", sanguchetto.verIngredientes());
		vista.addObject("total", sanguchetto.getPrecio());
		
		return vista;
	}
	
	@RequestMapping(value = "/eliminarIngrediente", method = RequestMethod.POST)
	public ModelAndView eliminarIngrediente(@RequestParam(value="ingrediente") String nombre) {
		
		ModelAndView vista = new ModelAndView("sangucheto/pedido");
		
		Ingrediente ingrediente = stock.obtenerIngredientePorNombre(nombre);
		if(sanguchetto.eliminarIngrediente(ingrediente)){
			stock.agregarStock(ingrediente, 1);
		}

		vista.addObject("stock", stock.obtenerStock());
		vista.addObject("condimentos", sanguchetto.verCondimentos());
		vista.addObject("ingredientes", sanguchetto.verIngredientes());
		vista.addObject("total", sanguchetto.getPrecio());
		
		return vista;
	}
}
