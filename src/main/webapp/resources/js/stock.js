jQuery(document).ready(function() {
    (function($) {

        $('[data-interactive="agregar"]').on('click', function() {
            var ingrediente = $(this).attr('data-value');
            var modal = document.querySelector('[data-interactive="modal"]');
            $('[data-interactive="ingrediente"]').text(ingrediente);
            jQuery('[name="ingrediente"]').val(ingrediente);
            modal.classList.remove("fade");
            modal.classList.add("modal");
            modal.classList.add("show");
        });

        $('[data-interactive="cerrar"]').on('click', function() {
            var modal = document.querySelector('[data-interactive="modal"]');
            $('[data-interactive="ingrediente"]').text('textContent');
            jQuery('[name="ingrediente"]').val('');
            jQuery('.spinner input').val(0);
            jQuery('[name="cantidad"]').val(0);
            modal.classList.add("fade");
            modal.classList.remove("modal");
            modal.classList.remove("show");
        });

        $('[data-interactive="up"]').on('click', function() {
            $('.spinner input').val(parseInt($('.spinner input').val(), 10) + 1);
            $('[name="cantidad"]').val($('.spinner input').val());

            if ($('.spinner input').val() > 0)
                document.querySelector('[data-interactive="agregarModal"]').removeAttribute('disabled');
        });

        $('[data-interactive="down"]').on('click', function() {
            if ($('.spinner input').val() > 0) {
                $('.spinner input').val(parseInt($('.spinner input').val(), 10) - 1);
                $('[name="cantidad"]').val($('.spinner input').val());
            }
            if ($('.spinner input').val() == 0)
                document.querySelector('[data-interactive="agregarModal"]').setAttribute('disabled', '');
        })
    })(jQuery);
});
