var total = 0;

var agregarIngrediente = function(ingrediente) {
    jQuery.ajax({
        type: "POST",
        url: '/sangucheto/agregarIngrediente',
        data: {'nombre': ingrediente},
        dataType: 'json'
    });

}

jQuery(document).ready(function() {
    (function($) {
        $('[data-interactive="dropdown"]').on('click', function() {
            var value = $(this).attr('data-value');
            $('[data-interactive="ingrediente"]').val(value);
            $('[data-interactive="formIngrediente"]').submit();
        });
    })(jQuery);
});