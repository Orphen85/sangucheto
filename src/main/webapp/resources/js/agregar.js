jQuery(document).ready(function() {
    (function($) {

        $('[data-interactive="up"]').on('click', function() {
            $('.spinner input').val(parseInt($('.spinner input').val(), 10) + 1);
            $('[name="cantidad"]').val($('.spinner input').val());

            if ($('.spinner input').val() > 0)
                document.querySelector('[data-interactive="agregarModal"]').removeAttribute('disabled');
        });

        $('[data-interactive="down"]').on('click', function() {
            if ($('.spinner input').val() > 0) {
                $('.spinner input').val(parseInt($('.spinner input').val(), 10) - 1);
                $('[name="cantidad"]').val($('.spinner input').val());
            }
            if ($('.spinner input').val() == 0)
                document.querySelector('[data-interactive="agregarModal"]').setAttribute('disabled', '');
        })
    })(jQuery);
});