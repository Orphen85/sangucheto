<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Stock</title>
		<c:import url="/WEB-INF/jsp/header.jsp" />
    </head>
    <body>
		  <div>
			  <table class="table table-bordered">
			    <thead>
			      <tr>
			        <th>Ingredientes</th>
			        <th>Condimentos</th>
			      </tr>
			    </thead>
			    <tbody>
			    <tr>
				    <td>
				    	 <table class="table">
<c:forEach items="${ingredientes}" var="ingrediente">
							<tr>
								<td>${ingrediente.nombre}</td>
							</tr>
</c:forEach>
				    	 </table>
				    </td>
				    <td>
				    	 <table class="table">
<c:forEach items="${condimentos}" var="condimento">
							<tr>
								<td colspan="2">${condimento.nombre}</td>
							</tr>
</c:forEach>
				    	 </table>
			    	</td>
			    </tr>
				<tr>
					<td>Total:</td>
					<td class="precio">${total}</td>
				</tr>
			    </tbody>
			  </table>
		  </div>
		  <div>
			  <a href="/sangucheto"><button class="btn btn-default">Terminar</button></a>    
		  </div>
    </body>
</html>