<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Stock</title>
  		<c:import url="/WEB-INF/jsp/header.jsp" />
  		<link rel="stylesheet" href="/sangucheto/resources/css/pedido.css">
        <script type="text/javascript" src="/sangucheto/resources/js/jquery-min.js"></script>
        <script type="text/javascript" src="/sangucheto/resources/js/pedido.js"></script>
    </head>
    <body> 
    	<div>
    		<form action="/sangucheto/agregarIngrediente" method="POST" data-interactive="formIngrediente">
	    		<input type="hidden" data-interactive="ingrediente" name="ingrediente"/>
			</form>
			<div class="dropdown">
				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
				Escoja los Ingredientes
				<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu2">
					<li role="presentation" class="dropdown-header">Ingredientes</li>
<c:forEach items="${stock}" var="ingrediente">
	<c:if test="${ingrediente.value > 0}">
					<li role="presentation">
						<a class="dropdown_pedido" data-value="${ingrediente.key.nombre}" data-interactive="dropdown" role="menuitem" tabindex="-1" href="#">
							<div class="dropdown_pedido_nombre">${ingrediente.key.nombre}</div>
							<div class="dropdown_pedido_precio">&#36; ${ingrediente.key.precio}</div>
						</a>
					</li>
	</c:if>
</c:forEach>
				</ul>
			</div>
    	</div>
		  <div>
			  <table class="table table-bordered">
			    <thead>
			      <tr>
			        <th>Nombre</th>
			        <th>Precio</th>
			        <th></th>
			      </tr>
			    </thead>
			    <tbody>
		  <c:forEach items="${ingredientes}" var="ingrediente">
					<tr>
						<td>${ingrediente.nombre}</td>
						<td class="precio">${ingrediente.precio}</td>
						<td>
							<form action="/sangucheto/eliminarIngrediente" method="POST">
								<input type="hidden" name="ingrediente" value="${ingrediente.nombre}"/>
								<button class="button-eliminar btn btn-warning" type="submit">Eliminar</button>
							</form>
						</td>
					</tr>
			</c:forEach>
			 <c:forEach items="${condimentos}" var="condimento">
			 		<tr>
						<td>${condimento.nombre}</td>
						<td class="precio">${condimento.precio}</td>
						<td>
							<form action="/sangucheto/eliminarIngrediente" method="POST">
								<input type="hidden" name="ingrediente" value="${condimento.nombre}"/>
								<button class="button-eliminar btn btn-warning" type="submit">Eliminar</button>
							</form>
						</td>
					</tr>
			</c:forEach>
				<tr>
					<td colspan="2">Total a pagar:</td>
					<td class="precio">${total}</td>
				</tr>
			    </tbody>
			  </table>
		  </div>
		  <div>
			  <c:if test="${total > 0}">
			  <form action="/sangucheto/pedido" method="POST">
				  	<button class="btn btn-primary" type="submit">Confirmar Compra</button>
			  </form>
			  </c:if>
			  <form action="/sangucheto/pedido/vaciar" method="POST">
				    <button type="submit" class="btn btn-default">Cancelar</button>
			  </form>
		  </div>
    </body>
</html>