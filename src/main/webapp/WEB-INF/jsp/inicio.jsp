<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<c:import url="/WEB-INF/jsp/header.jsp" />
		<title>Inicio</title>
	</head>
	<body>
		<div id="contenedor">
		
			<div>
				<img alt="Logo" src="/sangucheto/resources/im/san.png">
			</div>
			<div>
				<a href="/sangucheto/pedido"><button class="btn btn-primary">Comprar</button></a>
			</div>
			<div>
				<a href="/sangucheto/stock"><button class="btn btn-primary">Ver Stock</button></a>
			</div>
		</div>
	</body>
</html>