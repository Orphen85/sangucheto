<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Stock</title>
		<c:import url="/WEB-INF/jsp/header.jsp" />
        <script type="text/javascript" src="/sangucheto/resources/js/agregar.js"></script>
    </head>
    <body>
		<form method="POST" action="/sangucheto/stock/agregarCantidad">
<c:if test="${error}">
		<div class="alert alert-danger" role="alert">
			<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			<span class="sr-only">Error:</span>
			${mensaje}
		</div>
</c:if>
		<div >
			<select name="ingrediente" class="form-control" data-interactive="ingrediente">
<c:forEach items="${stock}" var="ingrediente">
				<option value="${ingrediente.key.nombre}">${ingrediente.key.nombre}</option>
</c:forEach>
			</select>	
  		</div>
		<div>
			<div class="input-group spinner">
			    <input type="text" class="form-control" value="0"  disabled>
			    <div class="input-group-btn-vertical">
			      <button type="button" class="btn btn-default" data-interactive="up"><i class="fa fa-caret-up glyphicon glyphicon-chevron-up"></i></button>
			      <button type="button" class="btn btn-default" data-interactive="down"><i class="fa fa-caret-down glyphicon glyphicon-chevron-down"></i></button>
			    </div>
		    </div>
		</div>
		<div>
			<input type="hidden" name="cantidad" data-interactive="cantidadHidden"/>
			<button type="submit" class="btn btn-primary" data-interactive="agregarModal" disabled>Agregar</button>
			<a href="/sangucheto/stock"><button type="button" class="btn btn-default">Cancelar</button></a>
		</div>
			</form>
    </body>
</html>