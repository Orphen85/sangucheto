<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Stock</title>
        <c:import url="/WEB-INF/jsp/header.jsp" />
        <link rel="stylesheet" href="/sangucheto/resources/css/eliminar.css">
        <script type="text/javascript" src="/sangucheto/resources/js/jquery-min.js"></script>
    </head>
    <body class="bodyEliminar">	  
		<form method="POST" action="/sangucheto/stock/eliminar">
			<div >
				<select name="ingrediente" class="form-control">
<c:forEach items="${stock}" var="ingrediente">
					<option value="${ingrediente.key.nombre}">${ingrediente.key.nombre}</option>
</c:forEach>
				</select>	
	  		</div>
			<div>
		  		<button class="btn btn-warning" type="submit">Quitar</button>
			</div>
	  		<div>
				<a href="/sangucheto/stock">
					<button type="button" class="btn btn-default" value="Volver">Volver</button>
				</a>
			</div>
	  	</form>
    </body>
</html>