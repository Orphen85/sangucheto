<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Stock</title>
		<c:import url="/WEB-INF/jsp/header.jsp" />
		<link rel="stylesheet" href="/sangucheto/resources/css/agregar.css">
    </head>
    <body>
<c:if test="${error}">
		<div class="alert alert-danger" role="alert">
			<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			<span class="sr-only">Error:</span>
			${mensaje}
		</div>
</c:if>
		<div>
			<form class="navbar-form navbar-left" method="POST" name="ingrediente" action="/sangucheto/ingrediente">
				<div class="form-group">
					<select name="tipo" class="form-control">
				  		<option value="INGREDIENTE">Ingrediente</option>
				  		<option value="CONDIMENTO">Condimento</option>
				  	</select>
					<input type="text" class="form-control" name="nombre" placeholder="Nombre"/>
			  		<input type="text"class="form-control" name="precio" placeholder="Precio"/>
				</div>
				<button type="submit" class="btn btn-primary">Agregar</button>
				<a href="/sangucheto/stock"><button class="btn btn-default" type="button">Cancelar</button></a>
			</form>
		</div>
    </body>
</html>