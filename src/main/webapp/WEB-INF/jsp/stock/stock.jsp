<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Stock</title>
        <c:import url="/WEB-INF/jsp/header.jsp" />
        <link rel="stylesheet" href="/sangucheto/resources/css/stock.css">
    </head>
    <body>
	  <div>
		  <table class="table table-bordered">
		    <thead>
		      <tr>
		        <th>Nombre</th>
		        <th>Cantidad</th>
		      </tr>
		    </thead>
		    <tbody>
<c:forEach items="${stock}" var="ingrediente">
		      <tr>
		        <td>${ingrediente.key.nombre}</td>
		        <td>${ingrediente.value}</td>
		      </tr>
</c:forEach>
		    </tbody>
		  </table>
	  </div>
		<div>
			<a href="/sangucheto/ingrediente"><button class="btn btn-primary">Agregar ingrediente</button></a>
		</div>
		<div>
			<a href="/sangucheto/stock/eliminar"><button class="btn btn-primary">Eliminar ingrediente</button></a>
		</div>
		<div>
			<a href="/sangucheto/stock/agregarCantidad"><button class="btn btn-primary">Agregar stock</button></a>
		</div>
		<div>
			<a href="/sangucheto"><button class="btn btn-default">Volver</button></a>
		</div>
    </body>
</html>